package com.foodTruck.FoodTruck.controller;

import com.foodTruck.FoodTruck.model.Dishes;
import com.foodTruck.FoodTruck.repository.DishesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@CrossOrigin
public class DishesController {

    @Autowired
    private DishesRepository dishesRepository;

    private boolean IsValid(Dishes dish){
        if(dish.getName().equals("") || dish.getDescription().equals("") || dish.getPrice() == 0){
            return false;
        }
        return true;
    }

    private boolean NameAlreadyExists(Dishes dish){
        List<Dishes> allDishes = dishesRepository.findAll();

        for(int i=0 ; i < allDishes.size(); i++){
            if(allDishes.get(i).getName().equals(dish.getName())){
                return true;
            }
        }
        return false;
    }

    @GetMapping("/getAllDishes")
    public List<Dishes> GetAllDishes(){
        return dishesRepository.findAll();
    }

    @PostMapping("/addDish")
    @ResponseBody
    public String AddDish(@RequestBody Dishes dish){
        if(!IsValid(dish))
            return "Values can't be null !";

        if(NameAlreadyExists(dish))
            return "A dish with this name already exists !";

        dishesRepository.save(dish);
        return "Dish " + dish.getName() + " has been added !";
    }

    @PutMapping("/updateDish")
    public String UpdateDish(@RequestBody Dishes dish){

        if(!IsValid(dish))
            return "Values can't be null !";

        if(NameAlreadyExists(dish))
            return "A dish with this name already exists !";

        dishesRepository.save(dish);
        return "Dish " + dish.getName() + " has been updated ! You can reload the page to see it !";
    }

    @DeleteMapping("/dishes/{id}")
    public String DeleteDish(@PathVariable int id){
        dishesRepository.deleteById(id);
        return "Dish " + id + " has been deleted !";
    }

}
