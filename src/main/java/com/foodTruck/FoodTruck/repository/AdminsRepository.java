package com.foodTruck.FoodTruck.repository;

import com.foodTruck.FoodTruck.model.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminsRepository extends JpaRepository<Admin, Integer> {
  /*  @Query("SELECT u FROM admin where u.name = :adminName and u.password = :adminPwd")
    Admin loginAdmin(@Param("adminName") String name, @Param("adminPwd") String password); */
}
